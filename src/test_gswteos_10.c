#include "config.h"

#include <errno.h> // for EXIT_SUCCESS, EXIT_FAILURE
#include <stdio.h> // for printf, fprintf, stderr
#include <stdlib.h> // for strtod, exit

#include <gswteos-10.h> // for gsw_z_from_p

int main( int argc, char ** argv ) {
    double sea_water_pressure = 0;
    double depth = 1e3;

    sea_water_pressure = gsw_p_from_z( -depth, 90 );
    printf( "@ pole, D = %0.2f meters ==> P = %0.2f decibar\n",
            depth, sea_water_pressure );

    sea_water_pressure = gsw_p_from_z( -depth, 0 );
    printf( "@ equator, D = %0.2f meters ==> P = %0.2f decibar\n",
            depth, sea_water_pressure );

    depth = -gsw_z_from_p( sea_water_pressure, 0 );
    printf( "@ equator, P = %0.2f decibar ==> D = %0.2f meters\n",
            sea_water_pressure, depth );

    depth = -gsw_z_from_p( sea_water_pressure, 90 );
    printf( "@ pole, P = %0.2f decibar ==> D = %0.2f meters\n",
            sea_water_pressure, depth );

    exit( EXIT_SUCCESS );
}
